#!/usr/bin/env python
# coding: utf-8

# In[55]:


import json
import numpy as np
from PIL import Image
from torch.utils import data
from torch.utils.data import DataLoader
import os, time
import matplotlib.pyplot as plt
import itertools
import pickle
import imageio
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
from torchvision.utils import save_image
import random
import torchvision.utils as vutils

import evaluator
ev = evaluator.evaluation_model()

batch_size = 256
sample_size = 32
lr = 0.0002
train_epoch = 100
print_every = 1


label_dict = {"gray cube": 0, "red cube": 1, "blue cube": 2, "green cube": 3, "brown cube": 4, "purple cube": 5, "cyan cube": 6, "yellow cube": 7, "gray sphere": 8, "red sphere": 9, "blue sphere": 10, "green sphere": 11, "brown sphere": 12, "purple sphere": 13, "cyan sphere": 14, "yellow sphere": 15, "gray cylinder": 16, "red cylinder": 17, "blue cylinder": 18, "green cylinder": 19, "brown cylinder": 20, "purple cylinder": 21, "cyan cylinder": 22, "yellow cylinder": 23}

def readjson(filename):
    img_name = []
    labels = []
    with open(filename,'r') as load_f:
        load_dict = json.load(load_f)
        for i in load_dict.keys():
            img_name.append(i)
        for i in load_dict.values():
            label = ""
            for j in i:
                if label!="":
                    label +=","
                label += str(label_dict[j])
            labels.append(label)
    return img_name, labels



def read_testjson(filename):
    img_name = []
    labels = []
    with open(filename,'r') as load_f:
        load_dict = json.load(load_f)
        for i in load_dict:
            label = []
            for j in i:
                label.append(label_dict[j])
            labels.append(label)
    return labels


def getData(mode):
    if mode == 'train':
        img, label = readjson('train.json')
        return np.squeeze(img), np.squeeze(label)
    else:
        img, label = readjson('test.json')
        return np.squeeze(img), np.squeeze(label)


class CLEVRLoader(data.Dataset):
    def __init__(self, root, mode, transform):
        self.root = root
        self.img_name, self.label = getData(mode)
        self.mode = mode
        self.transform = transform
        print("> Found %d images..." % (len(self.img_name)))

    def __len__(self):
        return len(self.img_name)

    def __getitem__(self, index):
        path = os.path.join(self.root, self.img_name[index])
        label = self.label[index]
        img = Image.open(path).convert('RGB')
        #img = img.resize((64,64), Image.BILINEAR)
        #img = np.asarray(img).transpose(1, 0, 2) / 255.0
        #img = Image.fromarray(img)
        img = self.transform(img)

        return img, label
    
transform_train = transforms.Compose([
    transforms.Resize((64, 64)),
    transforms.ToTensor(),
    transforms.Normalize(mean=(0.5,0.5,0.5), std=(0.5,0.5,0.5))
])

train_data = CLEVRLoader('CLEVR_dataset', 'train', transform=transform_train)
train_loader = DataLoader(dataset=train_data, batch_size=batch_size)

for data in train_loader:
    img, label = data
    print('image size : ', img.shape)
    print('label size : ', len(label))
    break


# In[56]:


device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print('GPU Use : ',torch.cuda.is_available())

nc=3
nz = int(100)
ngf = int(64)
ndf = int(64)
c_size = 24
#if opt.batchSize % c_size != 0:
#    raise ValueError('batchSize must be divisible by c_size (%d)'%(c_size))


def saving_path(root_path):
    try:
        os.makedirs(root_path)
    except OSError:
        pass
    checkpoint_path = os.path.join(root_path, 'pth')
    result_path = os.path.join(root_path, 'result_img')
    try:
        os.makedirs(checkpoint_path)
        os.makedirs(result_path)
    except OSError:
        pass
    return checkpoint_path, result_path

# custom weights initialization called on netG and netD
def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    if classname.find('Linear') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)


class Generator(nn.Module):
    def __init__(self):
        super(Generator, self).__init__()
        self.main = nn.Sequential(
            # input is Z, going into a convolution
            nn.ConvTranspose2d(     nz, ngf * 8, 4, 1, 0, bias=False),
            nn.BatchNorm2d(ngf * 8),
            nn.ReLU(True),
            # state size. (ngf*8) x 4 x 4
            nn.ConvTranspose2d(ngf * 8, ngf * 4, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ngf * 4),
            nn.ReLU(True),
            # state size. (ngf*4) x 8 x 8
            nn.ConvTranspose2d(ngf * 4, ngf * 2, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ngf * 2),
            nn.ReLU(True),
            # state size. (ngf*2) x 16 x 16
            nn.ConvTranspose2d(ngf * 2,     ngf, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ngf),
            nn.ReLU(True),
            # state size. (ngf) x 32 x 32
            nn.ConvTranspose2d(    ngf,      nc, 4, 2, 1, bias=False),
            nn.Tanh()
            # state size. (nc) x 64 x 64
        )

    def forward(self, input):
        output = self.main(input)
        return output


class Discriminator(nn.Module):
    def __init__(self):
        super(Discriminator, self).__init__()
        self.main = nn.Sequential(
            # input is (nc) x 64 x 64
            nn.Conv2d(nc, ndf, 4, 2, 1, bias=False),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf) x 32 x 32
            nn.Conv2d(ndf, ndf * 2, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 2),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*2) x 16 x 16
            nn.Conv2d(ndf * 2, ndf * 4, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 4),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*4) x 8 x 8
            nn.Conv2d(ndf * 4, ndf * 8, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 8),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*8) x 4 x 4
        )
        self.discriminator = nn.Sequential(
            nn.Conv2d(ndf * 8, 1, 4, 1, 0, bias=False),
            nn.Sigmoid()
            
        )
        self.Q = nn.Sequential(
            nn.Linear(ndf * 8 * 4 * 4, 100),
            nn.ReLU(),
            nn.Linear(100, c_size)
        )

    def forward(self, input, with_Q=False):
        output = self.main(input)
        #print(output)
        if not with_Q:
            #print('o : ', output.shape)
            dis_out = self.discriminator(output)
            #print('d : ', dis_out.shape)
            out = dis_out.view(-1, 1).squeeze(1)
            #print(out)
            return out
        else:
            dis_out = self.discriminator(output)
            dis_out = dis_out.view(-1, 1).squeeze(1)
            # Q_out = self.Q(output.view(opt.batchSize, -1))
            return dis_out, output


class Q(nn.Module):
    def __init__(self):
        super(Q, self).__init__()
        self.Q = nn.Sequential(
            nn.Linear(ndf * 8 * 4 * 4, 100),
            nn.ReLU(),
            nn.Linear(100, c_size)
        )

    def forward(self, input, batch_size, with_Q=False):
        # print(torch.mean(self.Q[0].weight.data))
        Q_out = self.Q(input.view(batch_size, -1))
        return Q_out
    
def sample_noise(z_size, cond_cls, batch_size, fixed_noise=None):
    if fixed_noise is None:
        idx = np.random.randint(cond_cls, size=batch_size)
        #cond = np.zeros((batch_size, cond_cls))
        #cond[range(batch_size), idx] = 1.0
        #cond = torch.Tensor(cond)
        
        y_list=[]
        for i in range(batch_size):
            n = random.randint(1, 3)
            temp_y=[]
            for j in range(n):
                temp_y.append((torch.rand(1, 1) * 24).type(torch.LongTensor).squeeze())
            y_list.append(temp_y)
    
        cond=torch.zeros([batch_size, 24])
        for i in range(batch_size):
            summ = torch.zeros([24])
            for j in (y_list[i]):
                summ[int(j)] = 1
            cond[i] = summ

        noise = torch.randn(batch_size, z_size - cond_cls)
        z = torch.cat([cond.to(device), noise.to(device)], 1).view(batch_size, -1, 1, 1)
    else:
        
        #one_hot = np.zeros((batch_size, cond_cls))
        #one_hot[range(batch_size), idx] = 1.0
        #cond = torch.Tensor(one_hot).view(batch_size, -1, 1, 1)
        
        test_label = read_testjson('test.json')
        #print("Test Label : ", test_label)

        one_hot=torch.zeros([32, 24])
        for i in range(len(test_label)):
            summ = torch.zeros([24])
            for j in (test_label[i]):
                summ[int(j)] = 1
            one_hot[i] = summ
        cond = one_hot

        noise = fixed_noise
        z = torch.cat([cond.to(device), noise.to(device)], 1).view(32, -1, 1, 1)
    return z, cond


# In[57]:


def trainIters():
    netG = Generator().to(device)
    netG.apply(weights_init)
    netD = Discriminator().to(device)
    netD.apply(weights_init)
    netQ = Q().to(device)
    netQ.apply(weights_init)
    
    criterion_D = nn.BCELoss().to(device)
    criterion_Q = nn.MultiLabelSoftMarginLoss().to(device)
    #criterion_Q = nn.MSELoss().to(device)
    # setup optimizer
    optimizerD = optim.Adam(netD.parameters(), lr=2e-4, betas=(0.5, 0.999))
    optimizerG = optim.Adam([{'params':netG.parameters()}, {'params':netQ.parameters()}], lr=1e-3, betas=(0.5, 0.999))
    
    #mul = batch_size // c_size
    #fixed_noise = np.random.normal(size=(c_size, nz - c_size, 1, 1))#.repeat(mul, axis=0)
    #fixed_noise = torch.Tensor(fixed_noise, device=device)
    fixed_noise = torch.randn(32, nz - 24)
    
    real_label = 1
    fake_label = 0
    i = 0
    maxi = -1 
    for epoch in range(train_epoch):
        i=0
        for data, y_ in (train_loader):
            i += 1
            
            # (1) Update D 
            # train with real
            optimizerD.zero_grad()
            real_cpu = data.to(device)
            batch_size = real_cpu.size(0)
            label = torch.full((batch_size,), real_label, device=device)

            output = netD(real_cpu)
            errD_real = criterion_D(output, label)
            errD_real.backward()
            D_x = output.mean().item()
        
            # train with fake
            z, c_idx = sample_noise(nz, c_size, batch_size)
            
            fake = netG(z)
            label.fill_(fake_label)
            output = netD(fake.detach())
            errD_fake = criterion_D(output, label)
            errD_fake.backward()
            D_G_z1 = output.mean().item()

            errD = errD_real + errD_fake
            optimizerD.step()
            
             # (2) Update G & Q 
            optimizerG.zero_grad()
            label.fill_(real_label)  # fake labels are real for generator cost
            D_output, Q_input = netD(fake, with_Q=True)
            reconstruct_loss = criterion_D(D_output, label)
            D_G_z2 = D_output.mean().item()
            #print(Q_input.shape)
            Q_output = netQ(Q_input, batch_size)
            target = torch.FloatTensor(c_idx).to(device)
            
            q_loss = criterion_Q(Q_output, target)

            G_Q_loss = reconstruct_loss + q_loss
            G_Q_loss.backward()
            optimizerG.step()
            
            print('[%d/%d][%d/%d] Loss_D: %.4f Loss_G: %.4f Loss_Q: %.4f D(x): %.4f D(G(z)): %.4f / %.4f'
                % (epoch+1, train_epoch, i, len(train_loader),
                    errD.item(), reconstruct_loss.item(), q_loss.item(), D_x, D_G_z1, D_G_z2))
            if i % 10 == 0:
                #vutils.save_image(real_cpu,
                #        'real_samples.png',
                #        normalize=True, nrow=8)
                f_noise, y_label = sample_noise(nz, c_size, batch_size, fixed_noise)
                netG.eval()
                fake = netG(f_noise)
                netG.train()
                vutils.save_image(fake.detach(),
                        'fake_samples_epoch_%03d_idx_%3d.png' % (epoch+1, i+1),
                        normalize=True, nrow=8)
                acc = ev.eval(fake, y_label)
                print(acc)
                if acc > maxi:
                    vutils.save_image(fake.detach(),'max/max.png',normalize=True, nrow=8)
                    savefilename = 'max/d.tar'
                    torch.save({'fixed_noise' : fixed_noise ,'state_dict':netD.state_dict(),},savefilename)
                    savefilename = 'max/g.tar'
                    torch.save({'state_dict':netG.state_dict(),},savefilename)
                    maxi = acc
                #save_history(tmp_dict, errD.item(), reconstruct_loss.item(), q_loss.item(), D_x, D_G_z1, D_G_z2, history)
                #save_history(tmp_dict, errD.item(), reconstruct_loss.item(), q_loss.item(), D_x, D_G_z1, D_G_z2)

trainIters()
"""
print("============pretrain================")
netG = Generator().to(device)
netD = Discriminator().to(device)
netQ = Q().to(device)
    
criterion_D = nn.BCELoss().to(device)
criterion_Q = nn.MSELoss().to(device)

optimizerD = optim.Adam(netD.parameters(), lr=2e-4, betas=(0.5, 0.999))
optimizerG = optim.Adam([{'params':netG.parameters()}, {'params':netQ.parameters()}], lr=1e-3, betas=(0.5, 0.999))

checkpoint = torch.load('max/d.tar')
netD.load_state_dict(checkpoint['state_dict'])
fixed_noise = checkpoint['fixed_noise']
checkpoint2 = torch.load('max/g.tar')
netG.load_state_dict(checkpoint2['state_dict'])


f_noise, y_label = sample_noise(nz, c_size, batch_size, fixed_noise)
netG.eval()
fake = netG(f_noise)
netG.train()
acc = ev.eval(fake, y_label)
print("Acc : ", acc)
"""

# In[ ]:




