import os
import csv
import sys
import argparse
import numpy as np
import matplotlib.pyplot as plt

def readcsv(filename):
    list1 = []
    Score = []
    with open(filename, newline='') as csvFile:
        rows = csv.reader(csvFile, delimiter=",")
        for row in rows:
            list1.append(row)
        for i in range(0, len(list1)):
            Score.append(float(list1[i][1]))
    print('len',len(Score))
    return Score

def show(act_train_acc):
    epoch_num = np.arange(1, 1000000 + 1, 1000)
    plt.cla()
    plt.grid(True)
    plt.figure(figsize=(10, 8))
    plt.xlabel("Training Episodes")
    plt.ylabel("Episode Score")
    plt.title('2048 TD Learning')
    pretrain_train, = plt.plot(epoch_num, act_train_acc)

    plt.savefig('result.jpg')

Score = readcsv("afterstate.csv")
show(Score)

print("Max epoch : {} score : {}".format((Score.index(max(Score))+1) * 1000, max(Score)))
