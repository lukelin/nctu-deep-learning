#!/usr/bin/env python
# coding: utf-8

import torch
import argparse
from torch.utils.data import TensorDataset, DataLoader
import dataloader
import matplotlib.pyplot as plt
import numpy as np

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
parser = argparse.ArgumentParser(description = 'Pytorch EGGNet')
parser.add_argument('--batchsize', type = int, default = 540, metavar = 'N', help = 'input batch size for training(defalut: 540)')
parser.add_argument('--epochs', type = int, default = 300, metavar='E', help = 'number of epochs to train (default: 300)')
parser.add_argument('--lr', type = float, default = 0.005, metavar='LR', help = 'learning rate (default: 0.005)')
#parser.add_argument('--no-cuda', action = 'store_true', default = False, help = 'disables CUDA training')
parser.add_argument('--seed', type=int, default = 1, metavar='S', help = 'random seed (default: 1)')

args = parser.parse_args()

(train_data, train_label, test_data, test_label) = dataloader.read_bci_data()
train_data = torch.from_numpy(train_data)
train_label = torch.from_numpy(train_label)
test_data = torch.from_numpy(test_data)
test_label = torch.from_numpy(test_label)
train_loader = DataLoader(TensorDataset(train_data, train_label), batch_size = args.batchsize)
test_loader = DataLoader(TensorDataset(test_data, test_label), batch_size = args.batchsize)

class EGGNet(torch.nn.Module):
    def __init__(self, act):
        super(EGGNet, self).__init__()
        #firstConv
        self.conv1 = torch.nn.Conv2d(1, 16, kernel_size = (1, 51), stride = (1, 1), padding = (0, 25), bias = False)
        self.norm1 = torch.nn.BatchNorm2d(16, eps = 1e-5, momentum = 0.1, affine = True, track_running_stats = True)
        #depthwiseConv
        self.conv2 = torch.nn.Conv2d(16, 32, kernel_size = (2, 1), stride = (1, 1), groups = 16, bias = False)
        self.norm2 = torch.nn.BatchNorm2d(32, eps = 1e-5, momentum = 0.1, affine = True, track_running_stats = True)
        #act1
        self.pool1 = torch.nn.AvgPool2d(kernel_size = (1, 4), stride = (1, 4), padding = 0)
        self.drop1 = torch.nn.Dropout(p = 0.25)
        #separableConv
        self.conv3 = torch.nn.Conv2d(32, 32, kernel_size = (1, 15), stride = (1, 1), padding = (0, 7), bias = False)
        self.norm3 = torch.nn.BatchNorm2d(32, eps = 1e-5, momentum = 0.1, affine = True, track_running_stats = True)
        #act2
        self.pool2 = torch.nn.AvgPool2d(kernel_size = (1, 8), stride = (1, 8), padding = 0)
        self.drop2 = torch.nn.Dropout(p = 0.25)
        #classify
        self.linear1 = torch.nn.Linear(in_features = 736, out_features = 2, bias = True)
        
        #activation function
        if act == 'ELU':
            self.activate = torch.nn.ELU(alpha = 0.005)
        elif act == 'LeakyReLU':
            self.activate = torch.nn.LeakyReLU()
        else:
            self.activate = torch.nn.ReLU()
        
        
    def forward(self, x):
        #firstConv
        out = self.norm1(self.conv1(x))
        #depthwiseConv
        out = self.norm2(self.conv2(out))
        out = self.activate(out)
        out = self.drop1(self.pool1(out))
        #separableConv
        out = self.norm3(self.conv3(out))
        out = self.activate(out)
        out = self.drop2(self.pool2(out))
        #classify
        out = out.view(out.size(0), -1)
        out = self.linear1(out)
        
        return out
    
class DeepConvNet(torch.nn.Module):
    def __init__(self, act):
        super(DeepConvNet, self).__init__()
        
        self.conv1 = torch.nn.Conv2d(1, 25, kernel_size = (1, 5), stride = (1, 1), padding = 0 , bias = False)
        self.conv2 = torch.nn.Conv2d(25, 25, kernel_size = (2, 1), stride = (1, 1), padding = 0, bias = False)
        self.norm1 = torch.nn.BatchNorm2d(25, eps = 1e-5, momentum = 0.1, affine = True, track_running_stats = True)
        #act1
        self.pool1 = torch.nn.MaxPool2d(kernel_size = (1, 2), stride = (1, 2), padding = 0)
        self.drop1 = torch.nn.Dropout(p = 0.5)
        
        self.conv3 = torch.nn.Conv2d(25, 50, kernel_size = (1, 5), stride = (1, 1), padding = 0 , bias = False)
        self.norm2 = torch.nn.BatchNorm2d(50, eps = 1e-5, momentum = 0.1, affine = True, track_running_stats = True)
        #act2
        self.pool2 = torch.nn.MaxPool2d(kernel_size = (1, 2), stride = (1, 2), padding = 0)
        self.drop2 = torch.nn.Dropout(p = 0.5)
        
        self.conv4 = torch.nn.Conv2d(50, 100, kernel_size = (1, 5), stride = (1, 1), padding = 0 , bias = False)
        self.norm3 = torch.nn.BatchNorm2d(100, eps = 1e-5, momentum = 0.1, affine = True, track_running_stats = True)
        #act3
        self.pool3 = torch.nn.MaxPool2d(kernel_size = (1, 2), stride = (1, 2), padding = 0)
        self.drop3 = torch.nn.Dropout(p = 0.5)
        
        self.conv5 = torch.nn.Conv2d(100, 200, kernel_size = (1, 5), stride = (1, 1), padding = 0 , bias = False)
        self.norm4 = torch.nn.BatchNorm2d(200, eps = 1e-5, momentum = 0.1, affine = True, track_running_stats = True)
        #act3
        self.pool4 = torch.nn.MaxPool2d(kernel_size = (1, 2), stride = (1, 2), padding = 0)
        self.drop4 = torch.nn.Dropout(p = 0.5)
        #flattern
        self.linear1 = torch.nn.Linear(in_features = 8600, out_features = 2, bias = True)
        
        #activation function
        if act == 'ELU':
            self.activate = torch.nn.ELU(alpha = 0.005)
        elif act == 'LeakyReLU':
            self.activate = torch.nn.LeakyReLU()
        else:
            self.activate = torch.nn.ReLU()
            
    def forward(self, x):
        
        out = self.conv2(self.conv1(x))
        out = self.norm1(out)
        out = self.activate(out)
        out = self.drop1(self.pool1(out))
        
        out = self.conv3(out)
        out = self.norm2(out)
        out = self.activate(out)
        out = self.drop2(self.pool2(out))
        
        out = self.conv4(out)
        out = self.norm3(out)
        out = self.activate(out)
        out = self.drop3(self.pool3(out))
        
        out = self.conv5(out)
        out = self.norm4(out)
        out = self.activate(out)
        out = self.drop4(self.pool4(out))
        
        out = out.view(out.size(0), -1)
        out = self.linear1(out)
        
        return out
             
    
def adjust_learning_rate(optimizer, epoch):
    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
    lr = args.lr * (0.1 ** (epoch // 300))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr
    
def train(epoch):
    model.train()
    train_loss = 0
    correct = 0
    
    for batch_idx, (data, label) in enumerate(train_loader):
        
        optimizer.zero_grad()
        output = model(data)
        loss = Loss(output, label.long())
        train_loss += loss.data[0]
        pred = output.data.max(1)[1]
        correct += pred.eq(label.data.long()).cpu().sum()
        
        loss.backward()
        optimizer.step()
        print('Train Epoch" {} [{}/{} ({:.0f}%)]\tLoss : {:.6f}'.format(epoch, (batch_idx+1) * len(data), len(train_loader.dataset),100.0 * (batch_idx+1) / len(train_loader), loss.data[0]))
    train_loss = train_loss
    train_loss /= len(train_loader)
    train_acc.append(100.*correct / len(train_loader.dataset))
    
def test(epoch):
    model.eval()
    test_loss = 0
    correct = 0
    
    for batch_idx, (data, label) in enumerate(test_loader):
        with torch.no_grad():
            output = model(data)
            
        test_loss += Loss(output, label.long()).data[0]
        pred = output.data.max(1)[1]
        correct += pred.eq(label.data.long()).cpu().sum()
        
    test_loss = test_loss
    test_loss /= len(test_loader)
    #print(100 * (float(correct) / len(train_loader.dataset)))
    test_acc.append(100 * (float(correct) / len(train_loader.dataset)))
    
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.2f}%)\n'.format(test_loss, correct, len(test_loader.dataset),  100 * (float(correct) / len(train_loader.dataset))))
    
def show(act_train_acc, act_test_acc, title):
    epoch_num = np.arange(1, args.epochs + 1, 1)
    plt.cla()
    plt.grid()
    plt.figure(figsize=(10, 8))
    #x_ticks = np.arange(1, args.epochs + 1, 50)
    #plt.xticks(x_ticks)
    plt.xlabel("Epoch")
    plt.ylabel("Accuracy(%)")
    plt.title('Result Comparison (' + title + ')')
    elu_train, = plt.plot(epoch_num, act_train_acc[0])
    elu_test, = plt.plot(epoch_num, act_test_acc[0])
    leaky_train, = plt.plot(epoch_num, act_train_acc[1])
    leaky_test, = plt.plot(epoch_num, act_test_acc[1])
    relu_train, = plt.plot(epoch_num, act_train_acc[2])
    relu_test, = plt.plot(epoch_num, act_test_acc[2])
    
    plt.legend(handles = [elu_train, elu_test, leaky_train, leaky_test, relu_train, relu_test,], labels = ['elu_train', 'elu_test', 'leaky_relu_train', 'leaky_relu_test', 'relu_train', 'relu_test'], loc = 4)
    plt.savefig(title + '.jpg')
    #plt.show()
    

print('cuda open : ',torch.cuda.is_available())
activation = ['ELU','LeakyReLU','ReLU']
train_acc = []
test_acc = []
act_train_acc = []
act_test_acc = []
maxi = 0 
#EGGNet

for i in activation:  
    print('************* EGGNet with {} *************'.format(i))
    model = EGGNet(i)
    model = model.double()
    Loss = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr = args.lr)

    #if args.cuda:
    #    device = torch.device('cuda')
    #    model.to(device)
    #args.epochs
    for epoch in range(1, args.epochs + 1):
        #adjust_learning_rate(optimizer,epoch)
        train(epoch)
        test(epoch)
        if maxi < float(max(test_acc)):
          maxi = float(max(test_acc))
          savefilename = 'EGG_model'+'.tar'
          torch.save({'epoch':epoch,'state_dict':model.state_dict(),},savefilename)
        
    act_train_acc.append(train_acc)
    act_test_acc.append(test_acc)
    train_acc = []
    test_acc = []

show(act_train_acc, act_test_acc, "EGGNet")
egg_elu_train = max(act_train_acc[0])
egg_elu_test = max(act_test_acc[0]) 
egg_leaky_train = max(act_train_acc[1])
egg_leaky_test = max(act_test_acc[1])
egg_relu_train = max(act_train_acc[2])
egg_relu_test = max(act_test_acc[2])


train_acc = []
test_acc = []
act_train_acc = []
act_test_acc = []
maxi = 0
#DeepConvNet

for i in activation:  
    print('************* DeepConvNet with {} *************'.format(i))
    model = DeepConvNet(i)
    model = model.double()
    Loss = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr = args.lr)

    #if args.cuda:
    #    device = torch.device('cuda')
    #    model.to(device)
    #args.epochs
    for epoch in range(1, args.epochs + 1):
        #adjust_learning_rate(optimizer,epoch)
        train(epoch)
        test(epoch)
        if maxi < float(max(test_acc)):
          maxi = float(max(test_acc))
          savefilename = 'Deep_model'+'.tar'
          torch.save({'epoch':epoch,'state_dict':model.state_dict(),},savefilename)
        
    act_train_acc.append(train_acc)
    act_test_acc.append(test_acc)
    train_acc = []
    test_acc = []

show(act_train_acc, act_test_acc, "DeepConvNet")

deep_elu_train = max(act_train_acc[0])
deep_elu_test = max(act_test_acc[0]) 
deep_leaky_train = max(act_train_acc[1])
deep_leaky_test = max(act_test_acc[1])
deep_relu_train = max(act_train_acc[2])
deep_relu_test = max(act_test_acc[2])

print("egg_elu_train : " ,egg_elu_train)
print("egg_elu_test : ",egg_elu_test)
print("egg_leaky_train : " ,egg_leaky_train)
print("egg_leaky_test : ",egg_leaky_test)
print("egg_relu_train : " ,egg_relu_train)
print("egg_relu_test : ",egg_relu_test)

print("deep_elu_train : " ,deep_elu_train)
print("deep_elu_test : ",deep_elu_test)
print("deep_leaky_train : " ,deep_leaky_train)
print("deep_leaky_test : ",deep_leaky_test)
print("deep_relu_train : " , deep_relu_train)
print("deep_relu_test : ",deep_relu_test)
"""
for i in activation:
    print('************* EGGNet with {} *************'.format(i))
    model = EGGNet(i)
    model = model.double()
    Loss = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr = args.lr)
    checkpoint = torch.load('EGG_model.tar')
    model.load_state_dict(checkpoint['state_dict'])
    epoch = checkpoint['epoch']
    test(0)
    
for i in activation:
    print('************* DeepNet with {} *************'.format(i))
    model = DeepConvNet(i)
    model = model.double()
    Loss = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr = args.lr)
    checkpoint = torch.load('Deep_model.tar')
    model.load_state_dict(checkpoint['state_dict'])
    epoch = checkpoint['epoch']
    test(0)
"""
