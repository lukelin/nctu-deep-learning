import os
import sys
import json
import torch
import argparse
from torch.utils.data import TensorDataset, DataLoader
import torchvision.models as models
import dataloader
import numpy as np
from dataloader import RetinopathyLoader
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import itertools

parser = argparse.ArgumentParser(description = 'Pytorch ResNet')
parser.add_argument('--model', type=int, default=18, help='ResNet18 or ResNet50')
#parser.add_argument('--trained', action = 'store_true', help = 'Model pretrained or not')
parser.add_argument('--batchsize', type = int, default = 4, help = 'input batch size for training (defalut: 4)')
parser.add_argument('--epochs', type = int, default = 10, help = 'number of epochs to train (default: 10)')
parser.add_argument('--lr', type = float, default = 1e-3, help = 'learning rate (default: 0.001)')
parser.add_argument('--optimizer', type = str, default = 'SGD', help = 'Adam or SGD (default: SGD)')
parser.add_argument('--momentum', type = float, default = 0.9, help = 'momentum fator (default: 0.9)')
parser.add_argument('--weight_decay', type = float, default = 5e-4, help = 'weight decay (default: 5e-4)')
parser.add_argument('--seed', type=int, default = 1, help = 'random seed (default: 1)')
args = parser.parse_args()

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print('GPU Use : ',torch.cuda.is_available())

train_data = RetinopathyLoader('data', 'train')
test_data = RetinopathyLoader('data', 'test')
#train_label = train_label.to(device)
#test_label = test_label.to(device)
testing_label = RetinopathyLoader('data', 'test').label
train_loader = DataLoader(dataset = train_data, batch_size=args.batchsize, shuffle=True)
test_loader = DataLoader(dataset = test_data, batch_size = args.batchsize)

class Basic_block(torch.nn.Module):
	out_extend = 1
	def __init__(self, input_size, output_size, stride = 1, downsample = None):
		super(Basic_block, self).__init__()
		self.conv1 = torch.nn.Conv2d(input_size, output_size, kernel_size = 3, stride = stride, padding = 1, bias = False)
		self.norm1 = torch.nn.BatchNorm2d(output_size)
		self.act1 = torch.nn.ReLU(inplace=True)

		self.conv2 = torch.nn.Conv2d(output_size, output_size, kernel_size = 3, padding = 1, bias = False)
		self.norm2 = torch.nn.BatchNorm2d(output_size)
		self.act2 = torch.nn.ReLU(inplace=True)

		self.downsample = downsample
		self.stride = stride

	def forward(self, x):
		out = self.conv1(x)
		out = self.norm1(out)
		out = self.act1(out)

		out = self.conv2(out)
		out = self.norm2(out)

		if self.downsample is not None:
			residual = self.downsample(x)
		else:
			residual = x

		out += residual
		out = self.act2(out)

		return out

class Bottleneck_block(torch.nn.Module):
	out_extend = 4
	def __init__(self, input_size, output_size, stride = 1,downsample = None):
		super(Bottleneck_block, self).__init__()
		self.conv1 = torch.nn.Conv2d(input_size, output_size, kernel_size = 1, bias = False)
		self.norm1 = torch.nn.BatchNorm2d(output_size)
		self.conv2 = torch.nn.Conv2d(output_size, output_size, kernel_size = 3, stride = stride, padding = 1, bias = False)
		self.norm2 = torch.nn.BatchNorm2d(output_size)
		self.conv3 = torch.nn.Conv2d(output_size, output_size*4, kernel_size = 1, bias = False)
		self.norm3 = torch.nn.BatchNorm2d(output_size*4)
		self.act = torch.nn.ReLU()
		self.downsample = downsample 
		self.stride = stride
	def forward(self, x):
		out = self.conv1(x)
		out = self.norm1(out)
		out = self.act(out)
		out = self.conv2(out)
		out = self.norm2(out)
		out = self.act(out)
		out = self.conv3(out)
		out = self.norm3(out)

		if self.downsample is not None:
			residual = self.downsample(x)
		else:
			residual = x

		out += residual
		out = self.act(out)

		return out


class ResNet(torch.nn.Module):
	def __init__(self, block, layers, class_num = 5, in_channel = 3):
		self.input_size = 64
		super(ResNet, self).__init__()
		self.conv1 = torch.nn.Conv2d(in_channel, 64, kernel_size = 7, stride = 2, padding = 3, bias = False)
		self.norm1 = torch.nn.BatchNorm2d(64)
		self.act1 = torch.nn.ReLU(inplace=True)
		self.pool1 = torch.nn.MaxPool2d(kernel_size = 3, stride = 2, padding = 1)
		
		self.layer1 = self.blocklayer(block, 64, layers[0])
		self.layer2 = self.blocklayer(block, 128, layers[1], stride = 2)
		self.layer3 = self.blocklayer(block, 256, layers[2], stride = 2)
		self.layer4 = self.blocklayer(block, 512, layers[3], stride = 2)

		self.pool2 = torch.nn.AvgPool2d(7, stride = 1)
		self.fc = torch.nn.Linear(51200 * block.out_extend, class_num)

	def blocklayer(self, block, channel, blocks, stride = 1): 
		downsample = None
		if stride != 1 or self.input_size != channel*block.out_extend:
			downsample = torch.nn.Sequential(
				torch.nn.Conv2d(self.input_size, channel*block.out_extend, kernel_size = 1, stride = stride, bias = False),
				torch.nn.BatchNorm2d(channel * block.out_extend),
			)

		layers = []
		layers.append(block(self.input_size, channel, stride, downsample))
		self.input_size = channel * block.out_extend
		for i in range(1, blocks):
			layers.append(block(self.input_size, channel))

		return torch.nn.Sequential(*layers)

	def forward(self, x):
		out = self.conv1(x)
		out = self.norm1(out)
		out = self.act1(out)
		out = self.pool1(out)

		out = self.layer1(out)
		out = self.layer2(out)
		out = self.layer3(out)
		out = self.layer4(out)
		
		out = self.pool2(out)

		res = out.view(out.size(0), -1)
		out = self.fc(res)
		return out


def train(epoch):
    model.train()
    train_loss = 0
    correct = 0
    
    for batch_idx, (data, label) in enumerate(train_loader):

    	data = data.type(torch.FloatTensor).to(device)
    	label = label.to(device)
    	optimizer.zero_grad()
    	output = model(data)
    	loss = Loss(output, label.long())
    	train_loss += loss.data[0]
    	pred = output.data.max(1)[1]
    	correct += pred.eq(label.data.long()).cpu().sum()
    	loss.backward()
    	optimizer.step()
    	print('Train Epoch" {} [{}/{} ({:.0f}%)]\tLoss : {:.6f}'.format(epoch, (batch_idx+1) * len(data), len(train_loader.dataset),100.0 * (batch_idx+1) / len(train_loader), loss.data[0]))
    train_loss = train_loss
    train_loss /= len(train_loader)
    train_acc.append(100.*correct / len(train_loader.dataset))
    
def test(epoch):
	predict = []
	model.eval()
	test_loss = 0
	correct = 0
	for batch_idx, (data, label) in enumerate(test_loader):
		#data, label = test_data[idx]
		data = data.type(torch.FloatTensor).to(device)
		#data = torch.unsqueeze(torch.tensor(data), dim=0).type(torch.FloatTensor).to(device)
		label = label.to(device)
		with torch.no_grad():
			output = model(data)
		test_loss += Loss(output, label.long()).data[0]
		pred = output.data.max(1)[1]
		for i in range(len(pred)):
			predict.append(int(pred[i]))
		correct += pred.eq(label.data.long()).cpu().sum()
	test_loss = test_loss
	test_loss /= len(test_loader)
	test_acc.append(100 * (float(correct) / len(test_loader.dataset)))
	print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.2f}%)\n'.format(test_loss, correct, len(test_loader.dataset),  100 * (float(correct) / len(test_loader.dataset))))
	return predict

def show(act_train_acc, act_test_acc, title):
    epoch_num = np.arange(1, args.epochs + 1, 1)
    plt.cla()
    plt.grid(True)
    plt.figure(figsize=(10, 8))
    #x_ticks = np.arange(1, args.epochs + 1, 50)
    #plt.xticks(x_ticks)
    plt.xlabel("Epoch")
    plt.ylabel("Accuracy(%)")
    plt.title('Result Comparison (' + title + ')')
    untrain_train, = plt.plot(epoch_num, act_train_acc[0], marker=".")
    untrain_test, = plt.plot(epoch_num, act_test_acc[0], marker=".")
    pretrain_train, = plt.plot(epoch_num, act_train_acc[1])
    pretrain_test, = plt.plot(epoch_num, act_test_acc[1])
    

    plt.legend(handles = [untrain_test, pretrain_test, untrain_train, pretrain_train,], labels = ['Test(w/o pretraining)', 'Test(with pretraining)', 'Train(w/o pretraining)', 'Train(with pretraining)'], loc = 2)
    plt.savefig(title + '.jpg')

def plot_confusion_matrix(predict, title='Confusion matrix'):
	plt.cla()
	target_names = [0,1,2,3,4]
	cm = confusion_matrix(testing_label, predict, target_names)
	cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
	accuracy = np.trace(cm) / np.sum(cm).astype('float')
	misclass = 1 - accuracy
	cmap = plt.get_cmap('Blues')

	#plt.figure(figsize=(8,10))
	plt.imshow(cm, interpolation='nearest', cmap=cmap)
	plt.title('Normalize confusion matrix')
	cb = plt.colorbar()
	tick_marks = np.arange(len(target_names))
	plt.xticks(tick_marks, target_names, rotation=45)
	plt.yticks(tick_marks, target_names)

	#cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
	thresh = cm.max() / 1.5# if normalize else cm.max() / 2
	for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
		plt.text(j, i, "{:0.2f}".format(cm[i, j]),	horizontalalignment="center",color="white" if cm[i, j] > thresh else "black")

	plt.tight_layout()
	plt.ylabel('True label')
	plt.xlabel('Predicted label\naccuracy={:0.4f}; misclass={:0.4f}'.format(accuracy, misclass))
	#plt.show()
	plt.savefig(title + '.jpg')
	cb.remove()
	print("finish drawing confusion matrix")

train_acc = []
test_acc = []
act_train_acc = []
act_test_acc = []
maxi = -1

if args.model == 18:
	model = ResNet(Basic_block,[2, 2, 2, 2])
	if args.optimizer == 'Adam':
		optimizer = torch.optim.Adam(model.parameters(), lr = args.lr, weight_decay = args.weight_decay)
	else:
		optimizer = torch.optim.SGD(model.parameters(), lr = args.lr, weight_decay = args.weight_decay, momentum = args.momentum)

	model.to(device)
	Loss = torch.nn.CrossEntropyLoss()
	for epoch in range(1, args.epochs + 1):
		train(epoch)
		predict = test(epoch)
		if maxi < float(max(test_acc)):
			maxi = float(max(test_acc))
			savefilename = 'ResNet18_model'+'.tar'
			torch.save({'epoch':epoch,'state_dict':model.state_dict(),},savefilename)
			plot_confusion_matrix(predict, 'Resnet_18_confusion')
	act_train_acc.append(train_acc)
	act_test_acc.append(test_acc)
	train_acc = []
	test_acc = []

	maxi = -1
	model = models.resnet18(pretrained = True)
	model.avgpool = torch.nn.AdaptiveAvgPool2d(1)
	model.fc = torch.nn.Linear(512, 5)
	if args.optimizer == 'Adam':
		optimizer = torch.optim.Adam(model.parameters(), lr = args.lr, weight_decay = args.weight_decay)
	else:
		optimizer = torch.optim.SGD(model.parameters(), lr = args.lr, weight_decay = args.weight_decay, momentum = args.momentum)

	model.to(device)
	Loss = torch.nn.CrossEntropyLoss()
	for epoch in range(1, args.epochs + 1):
		train(epoch)
		predict = test(epoch)
		if maxi < float(max(test_acc)):
			maxi = float(max(test_acc))
			savefilename = 'ResNet18_model_pretrain'+'.tar'
			torch.save({'epoch':epoch,'state_dict':model.state_dict(),},savefilename)
			plot_confusion_matrix(predict, 'Resnet_18_confusion_pretrain')
	act_train_acc.append(train_acc)
	act_test_acc.append(test_acc)
	train_acc = []
	test_acc = []

	show(act_train_acc, act_test_acc, "ResNet18")
else:
	maxi = -1
	model = ResNet(Bottleneck_block,[3, 4, 6, 3])
	if args.optimizer == 'Adam':
		optimizer = torch.optim.Adam(model.parameters(), lr = args.lr, weight_decay = args.weight_decay)
	else:
		optimizer = torch.optim.SGD(model.parameters(), lr = args.lr, weight_decay = args.weight_decay, momentum = args.momentum)
	model.to(device)
	Loss = torch.nn.CrossEntropyLoss()
	for epoch in range(1, args.epochs + 1):
		train(epoch)
		predict = test(epoch)
		if maxi < float(max(test_acc)):
			maxi = float(max(test_acc))
			savefilename = 'ResNet50_model'+'.tar'
			torch.save({'epoch':epoch,'state_dict':model.state_dict(),},savefilename)
			plot_confusion_matrix(predict, 'Resnet_50_confusion')
	act_train_acc.append(train_acc)
	act_test_acc.append(test_acc)
	train_acc = []
	test_acc = []

	maxi = -1
	model = models.resnet50(pretrained = True)
	model.avgpool = torch.nn.AdaptiveAvgPool2d(1)
	model.fc = torch.nn.Linear(512*4, 5)
	if args.optimizer == 'Adam':
		optimizer = torch.optim.Adam(model.parameters(), lr = args.lr, weight_decay = args.weight_decay)
	else:
		optimizer = torch.optim.SGD(model.parameters(), lr = args.lr, weight_decay = args.weight_decay, momentum = args.momentum)
	model.to(device)
	Loss = torch.nn.CrossEntropyLoss()
	for epoch in range(1, args.epochs + 1):
		train(epoch)
		test(epoch)
		if maxi < float(max(test_acc)):
			maxi = float(max(test_acc))
			savefilename = 'ResNet50_model_pretrain'+'.tar'
			torch.save({'epoch':epoch,'state_dict':model.state_dict(),},savefilename)
			plot_confusion_matrix(predict, 'Resnet_50_confusion_pretrain')
	act_train_acc.append(train_acc)
	act_test_acc.append(test_acc)
	train_acc = []
	test_acc = []

	show(act_train_acc, act_test_acc, "ResNet50")
"""
print("*********************** ResNet18 ***************************")

model = ResNet(Basic_block,[2, 2, 2, 2])
model.to(device)
Loss = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.SGD(model.parameters(), lr = args.lr, weight_decay = args.weight_decay, momentum = args.momentum)
checkpoint = torch.load('ResNet18_model.tar')
model.load_state_dict(checkpoint['state_dict'])
epoch = checkpoint['epoch']
test(0)

print("****************** ResNet18 pretrained**********************")

model = models.resnet18(pretrained = True)
model.avgpool = torch.nn.AdaptiveAvgPool2d(1)
model.fc = torch.nn.Linear(512, 5)
Loss = torch.nn.CrossEntropyLoss()
model.to(device)
optimizer = torch.optim.SGD(model.parameters(), lr = args.lr, weight_decay = args.weight_decay, momentum = args.momentum)
checkpoint = torch.load('ResNet18_model_pretrain.tar')
model.load_state_dict(checkpoint['state_dict'])
epoch = checkpoint['epoch']
test(0)

print("*********************** ResNet50 ***************************")

model = ResNet(Bottleneck_block,[3, 4, 6, 3])
model.to(device)
Loss = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.SGD(model.parameters(), lr = args.lr, weight_decay = args.weight_decay, momentum = args.momentum)
checkpoint = torch.load('ResNet50_model.tar')
model.load_state_dict(checkpoint['state_dict'])
epoch = checkpoint['epoch']
test(0)

print("****************** ResNet50 pretrained**********************")

model = models.resnet50(pretrained = True)
model.avgpool = torch.nn.AdaptiveAvgPool2d(1)
model.fc = torch.nn.Linear(512*4, 5)
Loss = torch.nn.CrossEntropyLoss()
model.to(device)
optimizer = torch.optim.SGD(model.parameters(), lr = args.lr, weight_decay = args.weight_decay, momentum = args.momentum)
checkpoint = torch.load('ResNet50_model_pretrain.tar')
model.load_state_dict(checkpoint['state_dict'])
epoch = checkpoint['epoch']
test(0)

"""




