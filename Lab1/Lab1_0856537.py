#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
import time


def sigmoid(x):
    """ Sigmoid function.
    This function accepts any shape of np.ndarray object as input and perform sigmoid operation.
    """
    return 1 / (1 + np.exp(-x))


def der_sigmoid(y):
    """ First derivative of Sigmoid function.
    The input to this function should be the value that output from sigmoid function.
    """
    return sigmoid(y) * (1 - sigmoid(y))


class GenData:
    @staticmethod
    def _gen_linear(n=100):
        """ Data generation (Linear)

        Args:
            n (int):    the number of data points generated in total.

        Returns:
            data (np.ndarray, np.float):    the generated data with shape (n, 2). Each row represents
                a data point in 2d space.
            labels (np.ndarray, np.int):    the labels that correspond to the data with shape (n, 1).
                Each row represents a corresponding label (0 or 1).
        """
        data = np.random.uniform(0, 1, (n, 2))

        inputs = []
        labels = []

        for point in data:
            inputs.append([point[0], point[1]])

            if point[0] > point[1]:
                labels.append(0)
            else:
                labels.append(1)

        return np.array(inputs), np.array(labels).reshape((-1, 1))

    @staticmethod
    def _gen_xor(n=100):
        """ Data generation (XOR)

        Args:
            n (int):    the number of data points generated in total.

        Returns:
            data (np.ndarray, np.float):    the generated data with shape (n, 2). Each row represents
                a data point in 2d space.
            labels (np.ndarray, np.int):    the labels that correspond to the data with shape (n, 1).
                Each row represents a corresponding label (0 or 1).
        """
        data_x = np.linspace(0, 1, n // 2)

        inputs = []
        labels = []

        for x in data_x:
            inputs.append([x, x])
            labels.append(0)

            if x == 1 - x:
                continue

            inputs.append([x, 1 - x])
            labels.append(1)

        return np.array(inputs), np.array(labels).reshape((-1, 1))

    @staticmethod
    def fetch_data(mode, n):
        """ Data gather interface

        Args:
            mode (str): 'Linear' or 'XOR', indicate which generator is used.
            n (int):    the number of data points generated in total.
        """
        assert mode == 'Linear' or mode == 'XOR'

        data_gen_func = {
            'Linear': GenData._gen_linear,
            'XOR': GenData._gen_xor
        }[mode]

        return data_gen_func(n)


class SimpleNet:
    def __init__(self, hidden_size1 = 5, hidden_size2 = 3, num_step=2000, print_interval=100,learning_rate=0.01):
        """ A hand-crafted implementation of simple network.

        Args:
            hidden_size:    the number of hidden neurons used in this model.
            num_step (optional):    the total number of training steps.
            print_interval (optional):  the number of steps between each reported number.
        """
        self.num_step = num_step
        self.print_interval = print_interval
        self.learning_rate = learning_rate

        # Model parameters initialization
        # Please initiate your network parameters here.
        self.hidden1_weights = np.random.rand(2, hidden_size1)
        self.hidden2_weights = np.random.rand(hidden_size1, hidden_size2) 
        self.hidden3_weights = np.random.rand(hidden_size2, 1) 

    @staticmethod
    def plot_result(data, gt_y, pred_y):
        """ Data visualization with ground truth and predicted data comparison. There are two plots
        for them and each of them use different colors to differentiate the data with different labels.

        Args:
            data:   the input data
            gt_y:   ground truth to the data
            pred_y: predicted results to the data
        """
        
        assert data.shape[0] == gt_y.shape[0]
        assert data.shape[0] == pred_y.shape[0]
        

        plt.figure()

        plt.subplot(1, 2, 1)
        plt.title('Ground Truth', fontsize=18)

        for idx in range(data.shape[0]):
            if gt_y[idx] == 0:
                plt.plot(data[idx][0], data[idx][1], 'ro')
            else:
                plt.plot(data[idx][0], data[idx][1], 'bo')

        plt.subplot(1, 2, 2)
        plt.title('Prediction', fontsize=18)

        for idx in range(data.shape[0]):
            if pred_y[idx] == 0:
                plt.plot(data[idx][0], data[idx][1], 'ro')
            else:
                plt.plot(data[idx][0], data[idx][1], 'bo')

        plt.show()

    def forward(self, inputs):
        """ Implementation of the forward pass.
        It should accepts the inputs and passing them through the network and return results.
        """
        self.x = inputs
        self.a1 = np.matmul(self.x, self.hidden1_weights)
        self.z1 = sigmoid(self.a1)
        
        self.a2 = np.matmul(self.z1, self.hidden2_weights)
        self.z2 = sigmoid(self.a2)
          
        self.a3 = np.matmul(self.z2, self.hidden3_weights)
        self.z3 = sigmoid(self.a3)
        
        return self.z3

    def backward(self):
        """ Implementation of the backward pass.
        It should utilize the saved loss to compute gradients and update the network all the way to the front.
        """
        
        grad_a3 = self.error * der_sigmoid(self.a3) 
        grad_w3 = np.matmul(self.z2.T, grad_a3)
        
        grad_a2 = np.matmul(grad_a3, self.hidden3_weights[:,:].T) * der_sigmoid(self.a2)
        grad_w2 = np.matmul(self.z1.T, grad_a2)
        
        grad_a1 = np.matmul(grad_a2, self.hidden2_weights[:,:].T) * der_sigmoid(self.a1)
        grad_w1 = np.matmul(self.x.T, grad_a1) 
        
        #np.clip(grad_w1, -1e2, 1e2, out=grad_w1)
        #np.clip(grad_w2, -1e2, 1e2, out=grad_w2)
        #np.clip(grad_w3, -1e2, 1e2, out=grad_w3)
        
        self.hidden1_weights -= self.learning_rate * grad_w1
        self.hidden2_weights -= self.learning_rate * grad_w2
        self.hidden3_weights -= self.learning_rate * grad_w3
        
    def train(self, inputs, labels):
        """ The training routine that runs and update the model.

        Args:
            inputs: the training (and testing) data used in the model.
            labels: the ground truth of correspond to input data.
        """
        # make sure that the amount of data and label is match
        assert inputs.shape[0] == labels.shape[0]

        n = inputs.shape[0]
        
        start = time.time()
        for epochs in range(self.num_step):
                #for idx in range(n):
                # operation in each training step:
                #   1. forward passing
                #   2. compute loss
                #   3. propagate gradient backward to the front
            self.output = self.forward(inputs)
            loss = (-1 * labels / self.output + (1-labels) / (1-self.output))
            self.error = loss
            #self.error = self.output - labels
            self.backward()
                
            if epochs % self.print_interval == 0:
                print('Epochs {}: '.format(epochs))
                acc = self.test(inputs, labels)
            if acc >= 99:
                print("\nConvergence at {} epochs !\n".format(epochs))
                break
        end = time.time()
            

        print('Training finished')
        print("Cost time : ", end - start)
        self.test(inputs, labels)

    def test(self, inputs, labels):
        """ The testing routine that run forward pass and report the accuracy.

        Args:
            inputs: the testing data. One or several data samples are both okay.
                The shape is expected to be [BatchSize, 2].
            labels: the ground truth correspond to the inputs.
        """
        n = inputs.shape[0]

        error = 0.0
        for idx in range(n):
            result = self.forward(inputs[idx:idx+1, :])
            error += abs(result - labels[idx:idx+1, :])
            
        error /= n
        
        print('error: ',float(error))
        
        print('accuracy: %.2f' % ((1 - error)*100) + '%')
        print('')
        
        return (1 - error)*100 


# In[2]:


if __name__ == '__main__':
    data, label = GenData.fetch_data('Linear', 100)
    #print(data.shape)
    net = SimpleNet(hidden_size1 = 5, hidden_size2 =3, num_step=100000,learning_rate= 0.01)
    net.train(data, label)

    pred_result = np.round(net.forward(data))
    
    SimpleNet.plot_result(data, label, pred_result)


# In[ ]:





# In[ ]:





# In[ ]:




